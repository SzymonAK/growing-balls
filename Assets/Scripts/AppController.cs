﻿using UnityEngine;
using System.Collections;

public class AppController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI()
	{
	
	}

	public void StartNewGame()
	{
		Application.LoadLevel("level");
	}

	public void Continue()
	{
		GamePrefs.prefs.Load();
		StartNewGame();
	}

	public void Quit()
	{
		Application.Quit();
	}
}
