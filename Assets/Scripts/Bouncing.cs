﻿using System;

using UnityEngine;
using System.Collections;

using Random = UnityEngine.Random;

public class Bouncing : MonoBehaviour
{
	public float Speed = 5;

	public bool colidedX { get; set; }
	public bool colidedY { get; set; }

	private Rigidbody2D body;

	void Start()
	{
		this.body = this.GetComponent<Rigidbody2D>();
		long randomX = 0;
		long randomY = 0;
		while (randomX == 0)
		{
			randomX = Random.Range(-10, 10);
		}
		while (randomY == 0)
		{
			randomY = Random.Range(-10, 10);
		}
		this.body.velocity = new Vector2(randomX, randomY).normalized * Speed;
	}

	void OnTriggerEnter2D(Collider2D coll)
	{
		var before = this.body.velocity;
		if (coll.gameObject.layer == LayerMask.NameToLayer("Ground"))
		{
			if (coll.gameObject.name == "Top" || coll.gameObject.name == "Bottom")
			{
				this.body.velocity = new Vector2(body.velocity.x, -body.velocity.y).normalized;
			}
			else
			{
				this.body.velocity = new Vector2(-body.velocity.x, body.velocity.y).normalized;
			}
		}
		else if (coll.gameObject.layer == LayerMask.NameToLayer("Enemy"))
		{
			var bounc = coll.GetComponent<Bouncing>();
			var collBody = coll.GetComponent<Rigidbody2D>();
			var beforeVelocity = collBody.velocity;

			if ((before.x < 0 && beforeVelocity.x > 0) || (before.x > 0 && beforeVelocity.x < 0) || colidedX)
			{
				bounc.colidedX = !colidedX;
				colidedX = false;
				this.body.velocity = new Vector2(-before.x, before.y);
			}
			if ((before.y < 0 && beforeVelocity.y > 0) || (before.y > 0 && beforeVelocity.y < 0) || colidedY)
			{
				bounc.colidedY = !colidedY;
				colidedY = false;
				this.body.velocity = new Vector2(before.x, -before.y);
			}
		}
		else if (coll.gameObject.layer == LayerMask.NameToLayer("Player"))
		{
			// nothing for now.
		}

	}

	void FixedUpdate()
	{
		this.body.velocity = this.body.velocity.normalized * Speed;
	}

	public void Initialize(float speedOfEnemy)
	{
		Speed = speedOfEnemy;
	}
}
