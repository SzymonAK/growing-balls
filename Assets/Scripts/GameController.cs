﻿using UnityEngine;
using System.Collections;
using Sys = System;

public class GameController : MonoBehaviour
{
	private const string FailsBallsText = "Fails: {0} of {1}";
	private const string AvailableBallsText = "Balls: {0}";
	private const string WinCoverageText = "{0:0.00} / {1} %";
	public Canvas PauseMenu;


	//public int NumberOfEnemy;
	//public float SpeedOfEnemy;
	//public float WinPercenntOfCoverage;
	//public int NumberOfPossibleFails;
	//public int NumberOfPlayerBalls;
	//public float GrowSpeed;
	//public float WeightGrowSpeed;
	
	
	//public GamePrefs Preferencies;
	public Vector2 DownLeftCorner;
	public Vector2 TopRightCorner;

	public GameObject EnemyBallPrefab;
	public GameObject PlayerBall;

	public GUIText FailsText;
	public GUIText BallsText;
	public GUIText StarText;
	public GUIText FailText;
	public GUIText PercentOfCoverText;


	public bool ButtonDown { get; private set; }
	public GameObject AcctualBall { get; private set; }
	private float gravityScale;

	private bool gameOn = false;
	private bool gameOver = false;
	private bool BallIsGrowing = false;
	private bool win = false;
	private int falls = 0;
	private int ballsUsed = 0;
	private float boxField;
	private float cover = 0;
	
	// Use this for initialization
	void Start ()
	{
		PauseMenu.enabled = false;
		FailsText.text = string.Format(FailsBallsText, 0, GamePrefs.prefs.NumberOfPossibleFails);
		BallsText.text = string.Format(AvailableBallsText, GamePrefs.prefs.NumberOfPlayerBalls);
		FailText.enabled = false;
		StarText.enabled = true;
		PercentOfCoverText.text = string.Format(WinCoverageText, 0, GamePrefs.prefs.WinPercenntOfCoverage);
		boxField = (TopRightCorner.x - DownLeftCorner.x) * (TopRightCorner.y - DownLeftCorner.y);
	}

	void Update()
	{
		if (!win && gameOver && Input.GetMouseButtonDown(0))
		{
			Debug.Log("level load");
			Application.LoadLevel(Application.loadedLevel);
		}
		if (win && gameOver && Input.GetMouseButtonDown(0))
		{
			Debug.Log("next level load");
			if(GamePrefs.prefs.NumberOfPlayerBalls > 9)
			{
				GamePrefs.prefs.NumberOfPlayerBalls--;
			}
			else if (GamePrefs.prefs.NumberOfPossibleFails > 2)
			{
				GamePrefs.prefs.NumberOfPossibleFails--;
			}
			else if (GamePrefs.prefs.NumberOfEnemy < 7)
			{
				GamePrefs.prefs.NumberOfEnemy += 1;
			}
			
			
			if (GamePrefs.prefs.SpeedOfEnemy < 7)
			{
				GamePrefs.prefs.SpeedOfEnemy += 0.2f;
			}

			if (GamePrefs.prefs.WinPercenntOfCoverage <= 100)
			{
				GamePrefs.prefs.WinPercenntOfCoverage += 5;
			}

			Application.LoadLevel(Application.loadedLevel);
		}
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Pause();
		}
	}

	void FixedUpdate()
	{
		if (Input.GetMouseButtonDown(0))
		{
			if (!PauseMenu.enabled && gameOn && !gameOver && ballsUsed < GamePrefs.prefs.NumberOfPlayerBalls)
			{
				StartCoroutine(this.GrowBall());
			}
			else if (!gameOn && !gameOver && !PauseMenu.enabled)
			{
				gameOn = true;
				this.SpawnEnemys();
				StarText.enabled = false;
			}
		}
	}


	IEnumerator GrowBall()
	{
		if (!BallIsGrowing)
		{
			BallIsGrowing = true;
			var ray = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			this.AcctualBall = Instantiate(PlayerBall, new Vector3(ray.x, ray.y, 0), Quaternion.identity) as GameObject;
			ballsUsed++;
			BallsText.text = string.Format(AvailableBallsText, GamePrefs.prefs.NumberOfPlayerBalls - ballsUsed);
			gravityScale = 0.1f;
			ButtonDown = true;
			while (ButtonDown)
			{
				float radius = 0.5f;
				if (Input.GetMouseButton(0))
				{
					if (this.AcctualBall != null)
					{
						this.AcctualBall.transform.localScale = Vector3.Lerp(
							this.AcctualBall.transform.localScale,
							this.AcctualBall.transform.localScale + new Vector3(GamePrefs.prefs.GrowSpeed, GamePrefs.prefs.GrowSpeed, GamePrefs.prefs.GrowSpeed),
							Time.deltaTime * 40);
						gravityScale += GamePrefs.prefs.WeightGrowSpeed;
					}

					yield return new WaitForSeconds(0.1f);
				}
				else
				{
					ButtonDown = false;
					if (AcctualBall != null)
					{
						radius *= this.AcctualBall.transform.localScale.x;
						cover += (radius * radius) * (float)Sys.Math.PI;
						PercentOfCoverText.text = string.Format(WinCoverageText, (cover * 100 / boxField), GamePrefs.prefs.WinPercenntOfCoverage);
						this.AcctualBall.GetComponent<Rigidbody2D>().gravityScale = gravityScale;
						this.AcctualBall.GetComponent<Rigidbody2D>().mass *= gravityScale;
					}
					this.AcctualBall = null;
					BallIsGrowing = false;
					if ((cover * 100 / boxField) >= GamePrefs.prefs.WinPercenntOfCoverage)
					{
						Win();
					}else if (ballsUsed >= GamePrefs.prefs.NumberOfPlayerBalls)
					{
						this.GameOver();
					}
				}
			}
		}
	}

	public void BallHited(GameObject hittedBall)
	{
		if (hittedBall.Equals(AcctualBall))
		{
			ButtonDown = false;
			BallIsGrowing = false;
			DestroyObject(hittedBall);
			AcctualBall = null;
			falls ++;
			FailsText.text = string.Format(FailsBallsText, falls, GamePrefs.prefs.NumberOfPossibleFails);
			if (falls == GamePrefs.prefs.NumberOfPossibleFails)
			{
				GameOver();
			}
		}
	}

	public void Pause()
	{
		PauseMenu.enabled = true;
		Time.timeScale = 0;
	}
	public void Resume()
	{
		PauseMenu.enabled = false;
		Time.timeScale = 1;
	}

	public void BackToMainMenu()
	{
		GamePrefs.prefs.Save();
		Application.LoadLevel("MainMenu");
	}

	public void Quit()
	{
		GamePrefs.prefs.Save();
		Application.Quit();
	}

	private void GameOver()
	{
		StarText.enabled = true;
		FailText.enabled = true;
		gameOn = false;
		gameOver = true;
	}

	private void Win()
	{
		FailText.text = "You WIN!! Next stage: ";
		FailText.enabled = true;
		StarText.enabled = true;
		gameOn = false;
		gameOver = true;
		win = true;
	}

	private void SpawnEnemys()
	{
		for (int i = 0; i < GamePrefs.prefs.NumberOfEnemy; i++)
		{
			GameObject enemy = Instantiate(EnemyBallPrefab, new Vector3(Random.Range(-7.5f, 7.6f), Random.Range(-4.8f, 4.8f), 0), Quaternion.identity) as GameObject;
			enemy.GetComponent<Bouncing>().Initialize(GamePrefs.prefs.SpeedOfEnemy);
		}
	}
}
