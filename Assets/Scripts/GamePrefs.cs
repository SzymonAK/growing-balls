﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GamePrefs : MonoBehaviour {
	public static GamePrefs prefs;
	
	public int NumberOfEnemy;
	public float SpeedOfEnemy;
	public float WinPercenntOfCoverage;
	public int NumberOfPossibleFails;
	public int NumberOfPlayerBalls;
	public float GrowSpeed;
	public float WeightGrowSpeed;

	void Awake()
	{
		if (prefs == null)
		{
			DontDestroyOnLoad(gameObject);
			prefs = this;
		}
		else if (prefs != this)
		{
			Destroy(gameObject);
		}
	}

	public void Save()
	{
		BinaryFormatter bf = new BinaryFormatter();
		FileStream fs = File.Open(Application.persistentDataPath + "/gamePrefs.dat", FileMode.OpenOrCreate);
		Prefs data = new Prefs
		{
			NumberOfEnemy = prefs.NumberOfEnemy,
			SpeedOfEnemy = prefs.SpeedOfEnemy,
			WinPercenntOfCoverage = prefs.WinPercenntOfCoverage,
			NumberOfPossibleFails = prefs.NumberOfPossibleFails,
			NumberOfPlayerBalls = prefs.NumberOfPlayerBalls,
			GrowSpeed = prefs.GrowSpeed,
			WeightGrowSpeed = prefs.WeightGrowSpeed
		};
		bf.Serialize(fs, data);
		fs.Close();
		Debug.Log("Save");
	}

	public void Load()
	{
		if (File.Exists(Application.persistentDataPath + "/gamePrefs.dat"))
		{
			Debug.Log("save exists");
			BinaryFormatter bf = new BinaryFormatter();
			FileStream fs = File.Open(Application.persistentDataPath + "/gamePrefs.dat", FileMode.Open);
			Prefs data = bf.Deserialize(fs) as Prefs;
			NumberOfEnemy = prefs.NumberOfEnemy;
			SpeedOfEnemy = prefs.SpeedOfEnemy;
			WinPercenntOfCoverage = prefs.WinPercenntOfCoverage;
			NumberOfPossibleFails = prefs.NumberOfPossibleFails;
			NumberOfPlayerBalls = prefs.NumberOfPlayerBalls;
			GrowSpeed = prefs.GrowSpeed;
			WeightGrowSpeed = prefs.WeightGrowSpeed;
			fs.Close();
		}

	}
}

[Serializable]
class Prefs
{
	public int NumberOfEnemy;
	public float SpeedOfEnemy;
	public float WinPercenntOfCoverage;
	public int NumberOfPossibleFails;
	public int NumberOfPlayerBalls;
	public float GrowSpeed;
	public float WeightGrowSpeed;
}
