﻿using UnityEngine;
using System.Collections;

public class EnemyColission : MonoBehaviour
{

	private GameController gameController;
	//private Rigidbody2D body;

	void Start()
	{
		GameObject gameControllerObject = GameObject.FindWithTag("GameController");
		if (gameControllerObject != null)
		{
			gameController = gameControllerObject.GetComponent<GameController>();
		}
		if (gameController == null)
		{
			Debug.Log("Cannot find 'GameController' script");
		}
		//body = GetComponent<Rigidbody2D>();
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if ((coll.gameObject.tag == "Enemy" || coll.gameObject.layer == LayerMask.NameToLayer("Ground")) && gameController.ButtonDown)
		{
			gameController.BallHited(gameObject);
		}
	}



	void OnCollisionStay2D(Collision2D coll)
	{
		if (coll.gameObject.layer == gameObject.layer)
		{
			if (coll.contacts.Length > 1) Debug.Log(coll.contacts.Length);
		}
		else if (coll.gameObject.layer == LayerMask.NameToLayer("Ground"))
		{

		}
	}

}
